<?php

namespace App\Issue\PhoneTopUp\Providers\Vtc;

class Lib{
    public function buildCheckoutUrl($order_code, $amount, $transaction_info)
    {
        // M?ng c�c tham s? chuy?n t?i VTC Pay

        $currency = 1;
        $param_extend = 'PaymentType:Bank';

        $plaintext = env('VTC_WEB_ID')  . "-" . $currency  . "-" . $order_code . "-" . $amount . "-" . env('VTC_RECEIVER_ACC') . "-" .$param_extend. "-" . env('VTC_KEY');
        $sign = hash('sha256', $plaintext);

        $data = "?website_id=" . env('VTC_WEB_ID')  ."&payment_method=" . $currency . "&order_code=" . $order_code . "&amount=" . $amount . "&receiver_acc=" .  env('VTC_RECEIVER_ACC');

        $data = $data . "&order_des=" . $transaction_info  ."&sign=" . $sign."&param_extend=" . $param_extend;
        $destinationUrl = env('VTC_PAY_URL') . $data;
        $destinationUrl = str_replace("%3a",":",$destinationUrl);
        $destinationUrl = str_replace("%2f","/",$destinationUrl);
        return $destinationUrl;
    }

    public function buildOrderStatusUrl($website_id, $order_code, $receiver_acc, $secret_key, $url,$port)
    {
        $plaintext="".$website_id . "-" . $order_code . "-" . $receiver_acc . "-" . $secret_key;
        $sign = strtoupper(hash('sha256', $plaintext));
        $xmlpush="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">" .
            "<soapenv:Header/>" .
            "<soapenv:Body>" .
            "<tem:CheckPartnerTransation>" .
            "<tem:website_id>".$website_id."</tem:website_id>" .
            "<tem:order_code>".$order_code."</tem:order_code>" .
            "<tem:receiver_acc>".$receiver_acc."</tem:receiver_acc>" .
            "<tem:sign>".$sign."</tem:sign>" .
            "</tem:CheckPartnerTransation>" .
            "</soapenv:Body>" .
            "</soapenv:Envelope>";
        $urlpar=parse_url($url);
        $headers = array(
            "POST ".$urlpar['path']." HTTP/1.1",
            "Host: ".$urlpar['host']."",
            "Content-Type: text/xml; charset=utf-8",
            "SOAPAction: VTCOnline.Card.WebAPI/Request",
            "Content-Length: ".strlen($xmlpush)
        );
        $ch = curl_init(); // initialize curl handle
        curl_setopt($ch, CURLOPT_VERBOSE, 1); // set url to post to
        curl_setopt($ch, CURLOPT_PORT, $port);
        curl_setopt($ch, CURLOPT_URL, $url); // set url to post to
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 40); // times out after 4s
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlpush); // add POST fields
        curl_setopt($ch, CURLOPT_POST, 1);
        $result = curl_exec($ch); // run the whole process
        return $result;
    }

    public function verifyPaymentUrl($data,$status, $sign)
    {
        // My plaintext
        $secret_key = env('VTC_KEY');
        $plaintext = $data . "|" . trim($secret_key);
        //print $plaintext;
        // M� h�a sign
        $verify_secure_code = strtoupper(hash('sha256', $plaintext));
        // X�c th?c ch? k� c?a ch? web v?i ch? k� tr? v? t? VTC Pay
        if ($verify_secure_code === $sign){
            return strval($status);
        }
        return false;
    }
}