<?php
namespace App\Issue\PhoneTopup\Providers;

class Direct{
	public function __construct()
    {
        
    }

    public function view()
    {
        \App::register('App\Issue\PhoneTopUp\PhoneTopUpServiceProvider');
        return 'phonetopup::direct';
    }
}