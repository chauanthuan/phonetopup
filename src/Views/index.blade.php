@extends('layouts.voucher')

@section('content')
    <div id="voucher_wrapper">
        <div class="voucher_header">
            <a class="logo" href="{{env('WEB_LINK')}}"><img src="../layouts/v2/images/mvoucher/logo_header.png"></a>
            <a href="{{env('WEB_LINK')}}">{{trans('content.what_gotit')}}</a>
            <div class="language_fs">
                <?php
                $browserLang = Request::server('HTTP_ACCEPT_LANGUAGE');
                $langPos = strpos($browserLang, 'vi');

                if(!$langPos){
                    $browserLang = 'en';
                }else{
                    $browserLang = 'vi';
                }

                $lang = Cookie::get('laravel_language', $browserLang);
                ?>
                @if( $lang == "vi")
                    <p><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/></p>
                @else
                    <p><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/></p>
                @endif
                <div class="drop_language" style="display:none;">
                    <a href="javascript:void(0)" class="" data-lang="vi"><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/> VI</a>
                    <a href="javascript:void(0)" class="" data-lang="en"><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/> EN</a>
                </div>
            </div>
        </div>
        <div class="voucher_body" style="position:relative">
        <!-- Check if is Normal voucher -->
            @if($voucher_tp != null)
                <div class="vc_template" style="">
                    <img src="{{env('IMG_SERVER').env('AWS_VOUCHER_TEMPLATE_FOLDER').'/'.$voucher_tp->top_image}}" height="" width="100%" style="margin:0 auto !important;display:block;">
                </div>
            @endif
            <div class="voucher_info">
                <div class="img">
                    @if($voucher->state == 4)
                        <p style="position:absolute;
                                        top:10px;
                                        right:10px;
                                        padding:5px 10px;
                                        background:#ff9500;
                                        color:#fff;
                                        font-weight:bold;
                                        border-radius:4px;
                                        font-size:14px;
                                        z-index:9;
                                        ">
                            {{trans('content.used')}}</p>
                    @endif
                    @if($voucher->state == 8)
                        <p style="position:absolute;
                                top:10px;
                                right:10px;
                                padding:5px 10px;
                                background:#FF5F5F;
                                color:#fff;
                                font-weight:bold;
                                font-size:14px;
                                border-radius:4px;
                                z-index:9;
                                ">
                            {{trans('content.v_expired')}}</p>
                    @endif
                    <img src="{{ Image::show($voucher->img_path)  }}">
                </div>
                <div class="cl"></div>
                <div class="product_info">

                    <div class="detail">
                        <div class="brand_logo">
                            <img src="{{ Image::show($product->brand->logo_img->img_path) }}" alt="">
                        </div>
                        <h3>{{$voucher->brand_name}}</h3>
                        <p>{{ Translate::transObj($product, 'name')   }}</p>
                        @if($psize != null)
                            <p>{{$psize}}</p>
                        @endif
                    </div>
                    <div class="cl"></div>
                    <div class="barcode">

                        @if($disableBarcode)
                        <p class="barcode disable">
                            <span>{{ trans('content.voucher_fb')}}</span>
                            @if(!isset($loginUrl))
                            <a class="loginfb_btn" href="javascript:void(0)" onClick="loginFb();"><i class="fa fa-facebook-square"></i>Login Facebook</a>
                            @else
                            <a href="{{ $loginUrl }}">
                                <img src="../layouts/v2/images/voucher/fblogin-button.png" alt="" style="opacity: 1;">
                            </a>
                            @endif
                        </p>
                        @else
                        <p>{{ trans('content.validity')." : ".date('d/m/Y', strtotime($voucher->expired_date))}}</p>

                                <span class="bar_code" style="display:none">
                                    <img src="<?php #echo '/voucher/barcode/'.$voucher->code?>" />
                                </span>
                        <p class="code" style="padding-top:20px">
                            {{-- $voucher->code --}}
                            <?php echo implode(" " , array(substr($voucher->code, 0,4),substr($voucher->code, 4,5),substr($voucher->code, 9,5) ));?>
                        </p>
                        @endif
                    </div>
                </div>
            </div>




            <div class="acor-info-vc voucher_note">
                <ul>
                    <li>
                        <h2>{{ trans('content.product_description') }}</h2>
                        <div>
                            <p>{!! Translate::transObj($product, 'desc') !!}</p>
                        <!-- <h4>{{ trans('content.how_it_work') }}</h4>
                                <p>{!! Translate::transObj($product, 'service_guide') !!}</p>
                                <br>
                                <h4>{{ trans('content.brand') }} : <a href="/brand/{{ $product->brand->name_slug }}">{{ Translate::transObj($product->brand, 'name') }}</a></h4>
                                <p>{{ trans('content.phone') }} : {{ $product->brand->phone }}</p>
                                <p>{{ trans('content.address') }} : {{ Translate::transObj($product->brand, 'address') }}</p> -->

                            <h3 class="viewMap find_store" style="display:none;"><img src="/layouts/v2/images/mvoucher/map_icon.png" height="20px"><a href="#" class="list_btn" data-brandid="{{$voucher->brand_id}}" data-storegroupid ="{{ $product->store_group_id }}">{{ trans('content.find_near')}}</a> &nbsp;</h3>
                        </div>
                    </li>
                <!-- <li>
                            <h2 class="viewMap"><a href="javascript:void(0)">{{ trans('content.show_store')}}</a></h2>
                        </li> -->
                    <li>
                        <h2>{{ trans('content.important')}}</h2>
                        <div>
                            <p>{{ trans('content.important_text')}}</p>
                        </div>
                    </li>
                    <li>
                        <h2>{{ trans('content.term_and_condition') }}</h2>
                        <div>
                            <p>{!! Translate::transObj($product, 'note') !!}</p>
                        </div>
                    </li>
                    <li>
                        <a href="{{env('WEB_LINK')}}/about.html"><h2>{{ trans('content.more_gotit')}}</h2></a>
                    </li>
                </ul>
            </div>

            <div class="send_again">
                <p>{{ trans('content.gift_easy')}}</p>
                <a class="send_gift" target="_blank" href="{{env('WEB_LINK')}}/product.html">{{ trans('content.send_a_gift')}}</a>

            <!-- <p>{{ trans('content.save_use_late') }}</p> -->
            {{--<a class="save_img" href="@if(isset($disableBarcode) && $disableBarcode == false) {{ env('IMG_SERVER').env('AWS_VOUCHER_FOLDER').'/'.$link.'-'.md5($voucher->voucher_id.$voucher->code).'_'.Session::get('laravel_language').'.png' }} @endif" @if(isset($disableBarcode) && $disableBarcode == false) {{ 'download="voucher_'.$link.'"' }} @endif >{{ trans('content.save_voucher') }}</a>--}}
            <!-- <a class="save_img" href="@if(isset($disableBarcode) && $disableBarcode == false) {{ route('voucher.save',['code'=>Crypt::encrypt($link.'|'.$voucher->voucher_id.$voucher->code)]) }} @endif" @if(isset($disableBarcode) && $disableBarcode == false) @endif >{{ trans('content.save_voucher') }}</a> -->
            </div>
        </div>
        <!-- Google map -->
        <div class="store">
            <div class="map-wrap" id="store_location">
                <div class="map active">
                    <div class="voucher_header">
                        <a class="logo" href="{{env('WEB_LINK')}}"><img src="../layouts/v2/images/mvoucher/logo_header.png" ></a>
                        <a href="{{env('WEB_LINK')}}">{{trans('content.what_gotit')}}</a>
                        <div class="language_fs">
                            <?php
                            $browserLang = Request::server('HTTP_ACCEPT_LANGUAGE');
                            $langPos = strpos($browserLang, 'vi');

                            if(!$langPos){
                            $browserLang = 'en';
                            }else{
                            $browserLang = 'vi';
                            }

                            $lang = Cookie::get('laravel_language', $browserLang);
                            ?>
                            @if( $lang == "vi")
                                <p><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/> VI</p>
                            @else
                                <p><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/> EN</p>
                            @endif
                            <div class="drop_language" style="display:none;">
                                <a href="javascript:void(0)" class="" data-lang="vi"><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/> VI</a>
                                <a href="javascript:void(0)" class="" data-lang="en"><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/> EN</a>
                            </div>
                        </div>
                    </div>
                    <div class="top_map">
                        <h3>
                            <img src="{{ Image::show($product->brand->logo_img->img_path) }}" alt="">
                            <?php echo mb_strimwidth($voucher->brand_name, 0, 25, "...", 'UTF-8');?>
                            {{--$voucher->brand_name--}}
                        </h3>
                        <span class="back_voucher"></span>
                        @if($vc_gotit)
                            <span class="show_more_brand"></span>
                        @endif
                    </div>
                    <div class="find_store">
                        <input name="brand_id_choose" type="hidden" value="">
                        <h3><img src="/layouts/v2/images/mvoucher/map_icon.png" height="20px"><a href="#" class="find" data-brandid="{{$voucher->brand_id}}" data-storegroupid ="{{ $product->store_group_id }}">{{ trans('content.find_near')}}</a></h3>
                    </div>
                    <div id="embed-map"></div>
                    <div class="add-wrap">
                        <div class="list-add">
                            <input type="hidden" name="store_group_id" value="{{ $product->store_group_id }}">
                            <ul>
                                <?php
                                $number = 0;
                                ?>
                                @foreach($product->stores() as $store)
                                    <?php //dd($store->address_vi)?>
                                    <li>
                                        <p class="goto-location" data-number="<?php echo $number ?>" style="cursor: pointer;">
                                            {{ Translate::transObj($store, 'brand_name') . " - " . Translate::transObj($store, 'name') }}<br/>
                                            <span><?php echo Translate::transObj($store, 'address') ?></span>
                                        <?php $browser = strtolower($_SERVER['HTTP_USER_AGENT']);?>
                                        @if(stripos($browser,'iphone') !== false || stripos($browser,'ipad') !== false)
                                            <!-- <a href="comgooglemaps://?q={{Translate::transObj($store,'address')}}&center={{$store->lat}},{{$store->long}}"> -->
                                                <a href="comgooglemaps://?daddr={{Translate::transObj($store,'address')}}&directionsmode=driving">
                                                @else
                                                    <!-- <a href="geo:{{$store->lat}},{{$store->long}}?q={{Translate::transObj($store,'address')}}"> -->
                                                        <a href="google.navigation:q={{Translate::transObj($store,'address')}}&{{$store->lat}},{{$store->long}}">
                                                            @endif
                                                            <img src="../layouts/v2/images/mvoucher/direct.png"></a>
                                        </p>
                                    </li>
                                    <?php
                                    $number++;
                                    ?>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        @if($list_brand != "")
            <div class="brand_ls_voucher active">
                <div class="voucher_header">
                    <a class="logo" href="{{env('WEB_LINK')}}"><img src="../layouts/v2/images/mvoucher/logo_header.png"></a>
                    <a href="{{env('WEB_LINK')}}">{{trans('content.what_gotit')}}</a>
                    <div class="language_fs">
                        <?php
                        $browserLang = Request::server('HTTP_ACCEPT_LANGUAGE');
                        $langPos = strpos($browserLang, 'vi');

                        if(!$langPos){
                        $browserLang = 'en';
                        }else{
                        $browserLang = 'vi';
                        }

                        $lang = Cookie::get('laravel_language', $browserLang);
                        ?>
                        @if( $lang == "vi")
                            <p><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/></p>
                        @else
                            <p><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/></p>
                        @endif
                        <div class="drop_language" style="display:none;">
                            <a href="javascript:void(0)" class="" data-lang="vi"><img src='../layouts/v2/images/lang-vi.png' style='width:25px !important;vertical-align:middle;'/> VI</a>
                            <a href="javascript:void(0)" class="" data-lang="en"><img src='../layouts/v2/images/lang-en.png' style='width:25px !important;vertical-align:middle;'/> EN</a>
                        </div>
                    </div>
                </div>
                <div class="top_brand">
                    <h3>{{ trans('content.all_brand')}}</h3>
                    <span class="back_store"></span>
                </div>
                <div class="band_list">
                    <p>{{ trans('content.choose_brand')}}</p>
                    <input type="hidden" name="store_group_id" value="{{$product->store_group_id}}">
                    @foreach($list_brand as $brand)
                        <div class="brand_item">
                            <a href="javascript:void(0)" data-id = "{{$brand->brand_id}}"><img class="logo-brand" src="{{ Image::show($brand->img_path) }}"/></a>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif

        <div class="voucher_footer">
            @if($voucher_tp != null)
                <div class="vc_template_foot" style="">
                    <img src="{{env('IMG_SERVER').env('AWS_VOUCHER_TEMPLATE_FOLDER').'/'.$voucher_tp->bottom_image}}" height="" width="100%" style="margin:0 auto !important; display:block;">
                </div>
            @endif
            <div class="footer_info">
                <div class="pull-left">
                    <a class="logo_footer" href="{{env('WEB_LINK')}}"><img src="../layouts/v2/images/mvoucher/logo_header.png" height="32px"></a>
                </div>
                <div class="pull-right">
                    <a href="{{env('WEB_LINK')}}">www.gotit.vn</a> &nbsp;|&nbsp; 1900 5588 20
                </div>
                
            </div>
        </div>
        <input name="img_server" type="hidden" value="{{ env('IMG_SERVER')}}">
    </div>

    <script type="text/javascript">
        <?php
        $googleMaps = array();
        foreach ($product->stores() as $store) {
        $googleMaps[] = array(
        'name'  => Translate::transObj($store, 'name'),
        'brand_name'  => Translate::transObj($store, 'brand_name'),
        'lat'   => $store->lat,
        'lng'   => $store->long,
        'address'   =>  Translate::transObj($store, 'address')
        );
        }
        echo 'var mapStores = '.json_encode($googleMaps);
        ?>
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjbeyHo86kWXc3nDZYK8q4AW5Joi0mvOY&v=3.exp&libraries=places"></script>

@endsection